"""
----------------------------------------------------------------------------------
 * Neural network Module
----------------------------------------------------------------------------------
"""
import autoencoder as ae
from build_network import NeuralNetworkArch
from test_network import NeuroTest
import softmax as sm
import f_measure as fm
import fine_tune as ft
import feedforward as ff
import mlp

"""
----------------------------------------------------------------------------------
 * Library
----------------------------------------------------------------------------------
"""
import pandas as pd
import numpy as np
import math
import os
import time


"""
----------------------------------------------------------------------------------
 * Module function in main program
----------------------------------------------------------------------------------
"""

def load_data(file):
    f = open(file,"r", encoding="utf8")
    data = f.readlines()
    f.close()
    return data

def copy_data_matrix(data):

    data_txt = data
    data_mtx = []

    for line in data_txt:
        data_col = line.split('\n')
        tmp_value = data_col[0]
        data_mtx.append(float(tmp_value))

    return data_mtx

def load_all_data(file):

    path = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()
    for i in dataset:
        data_col = i.split('\n')
        path.append(data_col[0])
    f.close()

    all_path = []
    for line in range(0,len(path)):
        for root, dirs, files in os.walk(path[line]):
            for filename in files:
                if filename[0:5] == "_NORM" :
                    all_path.append(path[line] + filename)
    return all_path

def load_predict_table(file):
    predict_class = []
    f = open(file, "r", encoding="utf8")
    dataset = f.readlines()

    for i in dataset:
        data_col = i.split('\t')
        predict_class.append(int(data_col[1]))
    f.close()

    return predict_class

def load_dataset(file):
    data_train = []

    dataset = load_data(file)

    for i in dataset:
        data_col = i.split('\t')
        data_train.append(str(data_col[0]))

    return data_train

def main():
    #-------------------------------------------------------------------------------------
    # * Hyperparameter for Neural network
    # :wdp - weight decay parameter
    # :s_param - sparsity parameter
    # :learning_rate - learning rate
    # :control_sparm - parameter for control sparsity
    #-------------------------------------------------------------------------------------
    wdp = 0.0001
    learning_rate = 0.01
    s_param = 0.01
    control_sparm = 0.001

    #-------------------------------------------------------------------------------------
    # * Parameter for Neural Network
    # :x_size - number m example for input feature
    # :h_size - number of hidden layer autoencoder neuron per layer
    # :encoder_size - number matrix of encoder [Hidden layer] number neuron per layer
    # :decoder_size - number matrix of decoder [Hidden layer] number neuron per layer
    # :hl_total - number of hidden layer
    # :y_size - number of output layer neuron ; m output feature equal m input feature
    #-------------------------------------------------------------------------------------
    x_size = 54675
    h_size = [500, 400, 300, 400, 500]
    encoder_size = [500, 400, 300]
    decoder_size = [300, 400, 500]
    hl_total = 3
    y_size = x_size

    #-------------------------------------------------------------------------------------
    # * Step of build network from neural network class
    # :NeuralNetworkArch() - class of neural network
    # :dnn - get Neural Network Class
    # :weights - weight for neural Network
    # :bias - bias for neural network
    #-------------------------------------------------------------------------------------
    print ("Build Network .....")
    dnn = NeuralNetworkArch(x_size, h_size, hl_total, y_size, encoder_size, decoder_size)
    weights = dnn.set_weight()
    bias = dnn.set_bias()

    #-------------------------------------------------------------------------------------
    # * Variable for Container
    # :input - matrix for variable input
    # :hidden_encoder - matrix for stage of encoder
    # :hidden_decoder - matrix for stage of decoder
    # :recontruction_x - matrix of recontruction input x
    # :sparsity - matrix for sparsity value container
    # :path - list of dataset path
    #-------------------------------------------------------------------------------------
    input = []
    hidden_encoder = []
    hidden_decoder = []
    recontruction_x = []
    sparsity = []
    path = load_dataset("train_data.txt")

    k = 12
    weight_classifier = sm.set_weight(k, 300)
    bias_classifier = sm.set_bias(k)
    output_target = load_predict_table("train_data.txt")
    predict_class = []
    xent = []
    epoch = 0

    print ("Forward Pass ......")


    for i in range(0,len(path)):

        start = time.time()
        h_enc = dnn.set_ench()
        h_dec = dnn.set_dech()

        _dW = dnn.set_derivative_weight()
        _dB = dnn.set_derivatives_bias()

        print ("----------------------------------------------------------------------------")
        print (" Step 1. Get dataset for input feature ... ...")
        input.append(copy_data_matrix(load_data(path[i])))

        print (" Step 2. Forward pass ... ...")
        encoder, decoder, _tmpRecon, error, wd, s, KL, mse, sparsity_penalty, ocf, rmse, wt = ff.train_forward(dnn, weights, bias, input[i], decoder_size, encoder_size, h_enc, h_dec, wdp, h_size, s_param, control_sparm)
        hidden_encoder.append(encoder)
        hidden_decoder.append(decoder)
        recontruction_x.append(_tmpRecon)
        sparsity.append(s)

        print (" Step 3. Fine tune with backpropagation ... ... \n")
        weights, bias =  ft.backward(recontruction_x[i], input[i], wt, weights, hidden_decoder[i], hidden_encoder[i], s_param, sparsity[i], control_sparm, wdp, _dW, _dB, bias, learning_rate  )
        end = time.time()

        print (" Summary ... ...")
        print (("\tInformasi Data ke %d ... ... ...") % (i))
        print (("\tName \t\t\t : %s") % (path[i]))
        print (("\tMean Square error \t : %s ") % (mse) )
        print (("\tRoot MSE          \t : %s ") % (rmse) )
        print (("\tOverral Cost func \t : %s ") % (ocf) )
        print (("\tSparsity penalty  \t : %s ") % (sparsity_penalty) )
        print (("\tComputation time  \t : %f minute ") % ((end - start)/60))
        print (("\tWeight example 0   \t : %g")%(weights[0][0][0]))
        print (("\tWeight example 1   \t : %g")%(weights[1][1][1]))
        print ("----------------------------------------------------------------------------")

        # check per seratur untuk epoch

        if i % 90 == 0 and i != 0 :

            _tpm_t = []
            _tpm_p = []
            for j in range(i-90,i):
                h_enc = hidden_encoder[j]
                input_x = h_enc[len(h_enc) - 1]
                _tpm_t.append(output_target[j])


                new_wc, new_bc, p_class, xent = sm.softmax_classifier(h_enc, input_x, weight_classifier, bias_classifier, output_target[j], k, wdp, learning_rate, xent, j)
                _tpm_p.append(p_class)
                weight_classifier = new_wc
                bias_classifier = new_bc
            print((" F- Measure for epoch %d")%(epoch))
            epoch +=1
            fm.f_measure(_tpm_p, _tpm_t, k)        

    for i in range(0,len(path)):

        h_enc = hidden_encoder[i]
        input_x = h_enc[len(h_enc) - 1]
        new_wc, new_bc, p_class, xent = sm.softmax_classifier(h_enc, input_x, weight_classifier, bias_classifier, output_target[i], k, wdp, learning_rate, xent, i)
        predict_class.append(p_class)

        weight_classifier = new_wc
        bias_classifier = new_bc
    fm.f_measure(predict_class, output_target, k)


    # TESTING

    x_test = 54675
    h_test = [500, 400, 300]
    ht_total = len(h_test)
    y_test = 300
    path = load_dataset("test_data.txt")

    dnn_test = NeuroTest(x_test, h_test, ht_total, y_test )
    output_target_test = load_predict_table("test_data.txt")

    input_test = []
    hidden_layer = []
    output_test = []
    predict_class_test = []

    for i in range(0,len(path)):

        hidden = dnn.set_hiddenlayer()
        input_test.append(copy_data_matrix(load_data(path[i])))



        hidden_layer.append(mlp.forward_pass(input_test[i], weights, hidden  , bias,   h_test))
        hidden = hidden_layer[i]
        print(hidden[len(h_test) - 1 ])
        output_test.append(mlp.forward_pass_output(hidden[len(h_test) - 1 ], weights,  bias,  dnn_test.set_output(), h_test))
        print(output_test)
        error = mlp.compute_cost_function(output_test[i], output_target)
        wd = mlp.count_weight_decay(weights,wdp)
        mse = mlp.overall_cost_function(error, wd)

    for i in range(0,len(path)):
        h_enc = []
        h_enc.append(output_test[i])
        print(h_enc)
        input_x = h_enc[len(h_enc) - 1]
        new_wc, new_bc, p_class, xent = sm.softmax_classifier(h_enc, input_x, weight_classifier, bias_classifier, output_target_test[i], k, wdp, learning_rate, xent, i)
        predict_class_test.append(p_class)

        weight_classifier = new_wc
        bias_classifier = new_bc
    fm.f_measure(predict_class_test, output_target_test, k)




if __name__ == '__main__':
    main()
