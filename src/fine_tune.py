import pandas as pd
import numpy as np
import math
import autoencoder as ae


def derivative_error(output, target):
    return -(target - output) * ae.sigmoid_derivative(output)

def derivative_hidden_layer(dE, weight):
    return weight * dE

def derivative_sparsity_contrain(sparam, sparsity, c_sparam):

    return c_sparam * ((-sparam / sparsity) + ((1-sparam) / (1-sparsity)))

def compute_dE(output, target):
    dE = np.zeros((len(output),1))
    for i in range(0,len(output)):
        dE[i] = derivative_error(output[i],target[i])
    return dE

def compute_dH(weights, delta, hidden_layer , sparam, sparsity, c_sparam):
    sum = 0
    dH = []

    for l in range(len(weights),1, -1):
        W = weights[l-1]
        hidden = hidden_layer[l-2]

        if l != len(weights) :
            _tmp2dH = _tmpdH
        _tmpdH = np.zeros((len(W[0]),1))
        for i in range(0,len(W[0])):
            for j in range(0,len(W)):
                if l == len(weights) :
                    sum += derivative_hidden_layer(delta[j] , W[j][i] )
                elif l != len(weights) :
                    sum += derivative_hidden_layer( _tmp2dH[j] , W[j][i] )
            _tmpdH[i] = (sum * derivative_sparsity_contrain(sparam, sparsity[l+1], c_sparam ) )  *  ae.sigmoid_derivative(hidden[i])
            sum = 0
        dH.append(_tmpdH)
    return dH


def compute_encdH(weights, wt, delta, hidden_layer , sparam, sparsity, c_sparam):
    sum = 0
    dH = []


    for l in range(len(weights),0, -1):
        if l == len(weights):
            W = wt[0]
        else :
            W = weights[l]

        hidden = hidden_layer[l-1]

        if l != len(weights) :
            _tmp2dH = _tmpdH
        _tmpdH = np.zeros((len(W[0]),1))
        for i in range(0,len(W[0])):
            for j in range(0,len(W)):
                if l == len(weights) :
                    sum += derivative_hidden_layer(delta[j] , W[j][i] )
                elif l != len(weights) :
                    sum += derivative_hidden_layer( _tmp2dH[j] , W[j][i] )
            _tmpdH[i] = ( sum *  derivative_sparsity_contrain(sparam, sparsity[l-1], c_sparam ) ) * ae.sigmoid_derivative(hidden[i])
            sum = 0
        dH.append(_tmpdH)
    return dH



def compute_weight_derivative(dH, _dW, dE, input, hidden_layer, wdp, weights):

    for l in range(0,len(_dW)):
        dW =  _dW[l]

        _tmpdH = dH[len(_dW) - (l+1) ]

        hidden = hidden_layer[l-1]
        for i in range(0,len(dW[0])):
            m = len(dW[0])
            for j in range(0,len(dW)):
                if l == 0 :
                    _dW[l][j][i] = (( _tmpdH[j] * input[i] ) / m ) + ( wdp * weights[l][j][i] )
                elif l != len(_dW) -1 :
                    _dW[l][j][i] = ((_tmpdH[j] * hidden[i] )/ m )+  ( wdp * weights[l][j][i] )
                elif l == len(_dW) -1 :
                    _dW[l][j][i] = (( dE[j] * hidden[i] )/ m )  + ( wdp * weights[l][j][i] )
    return _dW


def compute_bias_derivative(dH_enc, dH_dec, _dB, dE):

    for l in range(0,len(_dB)):
        dB =  _dB[l]
        if l < len(dH_enc):
            _tmpdH = dH_enc[len(dH_enc) - (l+1)]
        else:
            _tmpdH = dH_dec[len(dH_enc) - (l-1)]


        for i in range(0,len(dB[0])):
            m = len(dB[0])
            for j in range(0,len(dB)):
                if l == 0 :
                    _dB[l][j][i] = (( _tmpdH[j]  ) / m )
                elif l != len(_dB) -1  :
                    _dB[l][j][i] = ((_tmpdH[j]  )/ m )
                elif l == len(_dB) -1 :
                    _dB[l][j][i] = (( dE[j]  )/ m )
    return _dB

def update_weight(dW, weights, learning_rate):

    W = weights

    for l in range(0,len(W)):
        _tmpWeights = W[l]
        for i in range(0,len(_tmpWeights)):
            for j in range(0,len(_tmpWeights[0])):
                W[l][i][j] = W[l][i][j] - (learning_rate * dW[l][i][j])
    return W

def update_bias(dB, bias, learning_rate):

    B = bias
    for l in range(0,len(B)):
        _tmpBias = B[l]
        for i in range(0,len(_tmpBias)):
            for j in range(0,len(_tmpBias[0])):
                B[l][i][j] = B[l][i][j] - (learning_rate * dB[l][i][j])
    return B


"""
--------------------------------------------------------------------------------
 * Fine tune main algorithm
--------------------------------------------------------------------------------
"""

def backward(x_hat, x, wt, w, h_dec, h_enc, s_param, s, c_sparam, wdp, _dW, _dB, b, lr  ):

    dE = compute_dE(x_hat, x)
    dH_dec = compute_dH(wt, dE, h_dec, s_param, s, c_sparam )
    dH_enc = compute_encdH(w, wt ,dH_dec[len(dH_dec) - 1], h_enc, s_param, s, c_sparam )
    dW = compute_weight_derivative(dH_enc, _dW, dH_dec[len(dH_dec) - 1] , x, h_enc, wdp, w)
    dB = compute_bias_derivative(dH_enc, dH_dec, _dB, dE)
    weights_update = update_weight(dW, w, lr)
    bias = update_bias(dB, b, lr)

    return weights_update, bias
