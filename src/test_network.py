import pandas as pd
import numpy as np
import math

class NeuroTest(object):

    x_size = 0
    h_size = []
    hl_total = 0
    y_size = 0

    def __init__(self, x_size, h_size , hl_total, y_size):
        self.x_size = x_size
        self.h_size = h_size
        self.hl_total = hl_total
        self.y_size = y_size

    def get_xsize(self):
        return self.x_size

    def set_xsize(self, x_size):
        self.x_size = x_size

    def set_weight(self):
        """
        Catatan
        Jika nanti biasnya juga dikalikan dengan bobot naka self.x_size di tambah 1
        """
        weight_matrix = []
        for i in range(0,self.hl_total + 1):
            if i == 0 :
                #print (("kondisi 1  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.h_size[0] , self.x_size) * self.random_function(self.h_size[0], self.x_size) )
            elif i == self.hl_total  :
                #print (("kondisi 2  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.y_size ,self.h_size[i-1] ) * self.random_function(self.h_size[i-1], self.y_size) )
            else:
                #print (("kondisi 3  i : %d") % (i))
                weight_matrix.append(np.random.rand(self.h_size[i],self.h_size[i-1]) * self.random_function(self.h_size[i-1],self.h_size[i]) )
        return weight_matrix


    def random_function(self, n1, n2):
        return  math.sqrt( 2 / (n1 + n2))


    def set_hiddenlayer(self):
        hidden_layer = []
        for i in range(0,self.hl_total):
            hidden_layer.append(np.zeros((self.h_size[i])))
        return hidden_layer

    def set_output(self):
        return np.zeros((self.y_size))

    def set_bias(self):
        return np.random.rand(self.hl_total + 1)
