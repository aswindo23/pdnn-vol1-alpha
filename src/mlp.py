import pandas as pd
import numpy as np
import math

""" ===================== * FORWARD PASS * ========================== """

def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

def create_network( Weight, input, bias, n_hidden, n_input):
    """ Buat komputasi untuk menghitung jumlah network """

    network = np.zeros((n_hidden))
    sum = 0.0
    print(len(Weight))
    print(len(Weight[0]))
    print(len(input))
    print(n_hidden)
    for i in range(0,n_hidden):
        for j in range(0,n_input + 1):
            if j==n_input :
                #print ((" i : %d | j : %d") % (i,j))
                sum +=  bias[i]
                #sum += Weight[i][j] * bias
            else:
                #print ((" i : %d | j : %d") % (i,j))
                sum += Weight[i][j] * input[j]

        network[i] = sigmoid_function(sum)
        #print (("node %i : %f ") % (i,network[i]))
        sum = 0.0
    return network

def hypotheses(z):
    """
    hypotheses merupakan fungsi untuk aktifasi pada layer output dimana
    hypotheses Hw,b^(x) = a dan a merupakan hasil kalkulasi dari perhitungan
    fungsi aktifasi sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return sigmoid_function(z)

def cost_function(output,y):
    """
    cost function merupakan fungsi biaya yang mengkalkulasikan
    antara output target dengan output aktual dimana output actual adalah y
    dan output target dalah fungsi hypothesis
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    :y merupakan output aktual
    """
    return 0.5 * math.pow(abs(output - y ) , 2)

def count_weight_decay(weights, wdp):
    sum = 0
    for l in range(0,len(weights)):
        layer_weight = weights[l]
        for i in range(0,len(layer_weight[0])):
            for j in range(0,len(layer_weight)):
                sum += math.pow(layer_weight[j][i], 2)
    return (wdp*sum)/2

def overall_cost_function(m_cost_function, weight_decay):
    sum = 0
    for i in range(0,len(m_cost_function)):
        sum += m_cost_function[i]
    return (sum/len(m_cost_function)) + (weight_decay)

def forward_pass(input, weights, hidden_layer, bias,  h_size):

    for l in range(0, len(h_size) + 1 ):
        if l == 0 :
            #print ((" Hidden Layer | %d ") % (l))
            hidden_layer[l] = create_network(weights[l], input, bias[l], h_size[l], len(input))
        elif l != len(h_size) :
            #print ((" Hidden Layer | %d ") % (l))
            hidden_layer[l] = create_network(weights[l], hidden_layer[l-1], bias[l], h_size[l], len(hidden_layer[l-1]))
    return hidden_layer

def forward_pass_output(input, weights,  bias,  output, h_size):

    for l in range(0, len(h_size)  ):
        if l == len(h_size) -1  :
            #print ((" Output Layer | %d ") % (l))
            output = create_network(weights[l], input, bias[l], len(output), len(input))
    return output

def compute_cost_function(output, y_target):
    sum = 0
    error_cf = []
    for i in range(0,len(output)):
         error_cf.append(cost_function(output[i], y_target[i]))

    return error_cf


""" ===================== * BACKWARD PASS * ========================== """


def derivative_error(output, target):
    return -(target - output) * sigmoid_derivative(output)

def derivative_hidden_layer(dE, weight):
    return weight * dE

def compute_dE(output, target):
    dE = np.zeros((len(output),1))
    for i in range(0,len(output)):
        dE[i] = derivative_error(output[i],target[i])
    return dE

def compute_dH(weights, delta, hidden_layer ):
    sum = 0
    dH = []
    for l in range(len(weights),1, -1):
        W = weights[l-1]
        hidden = hidden_layer[l-2]
        if l != len(weights) :
            _tmp2dH = _tmpdH
        _tmpdH = np.zeros((len(W[0]),1))
        for i in range(0,len(W[0])):
            for j in range(0,len(W)):
                if l == len(weights) :
                    sum += derivative_hidden_layer(delta[j] , W[j][i] )
                elif l != len(weights) :
                    sum += derivative_hidden_layer( _tmp2dH[j] , W[j][i] )
            _tmpdH[i] = sum * sigmoid_derivative(hidden[i])
            sum = 0
        dH.append(_tmpdH)
    return dH

def compute_weight_derivative(dH, _dW, dE, input, hidden_layer, wd):

    for l in range(0,len(_dW)):
        dW =  _dW[l]
        _tmpdH = dH[len(_dW) - (l+2)]
        hidden = hidden_layer[l-1]
        for i in range(0,len(dW[0])):
            m = len(dW[0])
            for j in range(0,len(dW)):
                if l == 0 :
                    _dW[l][j][i] = (( _tmpdH[j] * input[i] ) / m ) + wd
                elif l != len(_dW) -1 :
                    _dW[l][j][i] = ((_tmpdH[j] * hidden[i] )/ m )+  wd
                elif l == len(_dW) -1 :
                    _dW[l][j][i] = (( dE[j] * hidden[i] )/ m )  + wd
    return _dW

def compute_bias_derivative(dH, _dB, dE, hidden_layer):

    for l in range(0,len(_dB)):
        dB =  _dB[l]
        _tmpdH = dH[len(_dB) - (l+2)]
        hidden = hidden_layer[l-1]
        for i in range(0,len(dB[0])):
            m = len(dB[0])
            for j in range(0,len(dB)):
                if l == 0 :
                    _dB[l][j][i] = (( _tmpdH[j]  ) / m )
                elif l != len(_dB) -1 :
                    _dB[l][j][i] = ((_tmpdH[j]  )/ m )
                elif l == len(_dB) -1 :
                    _dB[l][j][i] = (( dE[j]  )/ m )
    return _dB

def update_weight(dW, weights, learning_rate):

    W = weights

    for l in range(0,len(W)):
        _tmpWeights = W[l]
        for i in range(0,len(_tmpWeights)):
            for j in range(0,len(_tmpWeights[0])):
                W[l][i][j] = W[l][i][j] - learning_rate * dW[l][i][j]
    return W




def update_bias(dB, bias, learning_rate):

    B = bias
    for l in range(0,len(B)):
        _tmpBias = B[l]
        for i in range(0,len(_tmpBias)):
            for j in range(0,len(_tmpBias[0])):
                B[l][i][j] = B[l][i][j] - learning_rate * dB[l][i][j]
    return B
