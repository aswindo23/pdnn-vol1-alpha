import autoencoder as ae

def encoder_forward_pass(input, weights, hidden_layer, bias,  h_size):

    for l in range(0, len(h_size)  ):
        if l == 0 :
            hidden_layer[l] = ae.create_network(weights[l], input, bias[l], h_size[l], len(input))
        else :
            hidden_layer[l] = ae.create_network(weights[l], hidden_layer[l-1], bias[l], h_size[l], len(hidden_layer[l-1]))
    return hidden_layer

def decoder_forward_pass(input, weights, hidden_layer, bias,  h_size):
    for l in range(0, len(h_size)  ):
        if l == 0 :
            hidden_layer[l] = ae.create_network(weights[l], input, bias[l + len(h_size)], h_size[l+1], len(input))
        elif l != len(h_size) -1 :
            hidden_layer[l] = ae.create_network(weights[l], hidden_layer[l-1], bias[l + len(h_size)], h_size[l+1], len(hidden_layer[l-1]))
    return hidden_layer


def recontruction_input(input, weights,  bias,  output, h_size):

    for l in range(0, len(h_size)  ):
        if l == len(h_size) -1 :
            output, z = ae.create_network_output(weights[l], input, bias[l+len(h_size)], len(output), len(input))
    return output, z

def compute_cost_function(output, y_target):
    sum = 0
    error_cf = []
    for i in range(0,len(output)):
         error_cf.append(ae.cost_function(output[i], y_target[i]))

    return error_cf

def train_forward(dnn, weights, bias, input, decoder_size, encoder_size, h_enc, h_dec , wdp, h_size, s_param, control_sparm):

    encoder =  encoder_forward_pass(input, weights, h_enc  , bias,   encoder_size)
    Wt = ae.weights_transpose(weights)
    decoder = decoder_forward_pass(encoder[len(encoder)-1], Wt, h_dec  , bias,   decoder_size)
    _tmpRecon, z_output = recontruction_input(decoder[len(decoder) - 1 ], Wt,  bias,  dnn.set_output(), decoder_size)
    error = compute_cost_function(_tmpRecon, input )
    wd = ae.count_weight_decay(weights,wdp)
    sparsity = ae.sparsity_constrain( _tmpRecon, decoder, encoder, len(h_size) )
    KL = ae.KullbackLeibler(sparsity, s_param)
    mse, sparsity_penalty, ocf = ae.overall_cost_function(error, wd, control_sparm, KL)
    rmse = ae.rmse(mse)

    return encoder, decoder, _tmpRecon, error, wd, sparsity, KL, mse, sparsity_penalty, ocf, rmse, Wt
