import pandas as pd
import numpy as np
import math

def random_function( n1, n2):
    return  math.sqrt( 2 / (n1 + n2))

def set_weight(output_size, input_size):

    weight = np.random.rand(output_size , input_size) * random_function(output_size, input_size)
    return weight

def set_bias(output_size):

    bias = np.random.rand(output_size , 1)
    return bias

def sofmax_function(z):
    return math.exp(z)

def one_hot(y, k_class):

    y_target = np.zeros((k_class ))

    for j in range(0, k_class):
        if j == y :
            y_target[j] = 1
        else:
            y_target[j] = 0

    return y_target


def create_network( Weight, input, bias, n_hidden, n_input):
    """ Buat komputasi untuk menghitung jumlah network """

    network = np.zeros((n_hidden))
    sum = 0.0

    for i in range(0,n_hidden):
        for j in range(0,n_input + 1):
            if j==n_input :
                sum +=  bias[i]
            else:
                sum += Weight[i][j] * input[j]
        network[i] = sofmax_function(sum)
        sum = 0.0
    return network

def sum_hypothesis(output):

    sum = 0
    for i in range(0,len(output)):
        sum += output[i]
    return sum

def softmax_score(output, sum_softmax):

    score = np.zeros((len(output)))

    for i in range(0,len(output)):
        score[i] = output[i]/sum_softmax
    return score

def cross_entropy(score, y_target):

    sum = 0
    for i in range(0,len(score)):
        sum += math.log(score[i]) * y_target[i]

    return - sum

def to_classlabel(score):
    max = 0
    predict_class = 0
    for i in range(0,len(score)):
        if max < score[i] :
            max = score[i]
            predict_class = i

    return predict_class

def cost(cross_entropy, wd):
    sum = 0

    for i in range(0,len(cross_entropy)):
        sum += cross_entropy[i]

    return (sum/len(cross_entropy)) + (wd)

def cross_entropy_derivative(score, y_target):
    return (score - y_target)


def weight_derivative(score, y_target ,input, weight, wdp):

    weight_dev = np.zeros((len(weight), len(weight[0])))

    for i in range(0, len(weight)):
        for j in range(0, len(weight[i])):
            weight_dev[i][j] = ((cross_entropy_derivative(score[i],y_target[i]) * input[i]) / len(weight) + (wdp * weight[i][j]))

    return weight_dev

def update_weight(weight, dW, learning_rate):

    new_weight = weight

    for i in range(0,len(weight)):
        for j in range(0, len(weight[i])):
            new_weight[i][j] = weight[i][j] - learning_rate* dW[i][j]

    return new_weight


def bias_derivative(score, y_target , bias):

    bias_dev = np.zeros((len(bias), 1))

    for i in range(0, len(bias)):
            bias_dev[i] = cross_entropy_derivative(score[i],y_target[i]) / len(bias)

    return bias_dev


def update_bias(bias, dB, learning_rate):

    new_bias = bias

    for i in range(0,len(bias)):
            new_bias[i] = bias[i] - learning_rate* dB[i]

    return new_bias


def count_weight_decay(weights, wdp):
    sum = 0
    for i in range(0,len(weights)):
        for j in range(0,len(weights[i])):
            sum += math.pow(weights[i][j], 2)

    return (wdp*sum)/2

def softmax_classifier(encoder, input, wc, bc, t, k, wdp, lr, xent, i):



    #print (" \t one-hot encoding ... ...")
    true_label = one_hot(t, k)

    
    #print(" \t create network ... ...")
    net_in = create_network(wc, encoder[len(encoder) - 1], bc, k, len(encoder[len(encoder) - 1]))
    sum_score = sum_hypothesis(net_in)

    #print(" \t softmax activation")
    score = softmax_score(net_in, sum_score )

    #print(" \t expand probability to predict class")
    predict_class = to_classlabel(score)

    #print(" \t compute cross entropy")
    _tpmXent = cross_entropy(score , true_label )
    xent.append(_tpmXent)

    #print(" \t lost function cross entropy ")
    wd_classifier = count_weight_decay(wc,wdp)
    cost_value = cost(xent,wd_classifier)


    #print (" \t compute derivative lost function ")

    dWC = weight_derivative(score, true_label, input, wc, wdp)
    dBC = bias_derivative(score, true_label,  bc)

    #print (" update weight and bias for softmax classifier ")
    new_WEC = update_weight(wc, dWC, lr )
    new_BEC = update_bias(bc, dBC, lr )

    #print ("change old matrix weight and vector bias to new value ... ...")

    return new_WEC, new_BEC, predict_class, xent


def softmax_classifier_test(encoder, input, wc, bc, t, k, wdp, lr, xent, i):



    #print (" \t one-hot encoding ... ...")
    true_label = one_hot(t, k)

    #print(" \t create network ... ...")
    net_in = create_network(wc, encoder[len(encoder) - 1], bc, k, len(encoder[len(encoder) - 1]))
    sum_score = sum_hypothesis(net_in)

    #print(" \t softmax activation")
    score = softmax_score(net_in, sum_score )

    #print(" \t expand probability to predict class")
    predict_class = to_classlabel(score)

    #print(" \t compute cross entropy")
    _tpmXent = cross_entropy(score , true_label )
    xent.append(_tpmXent)

    #print(" \t lost function cross entropy ")
    wd_classifier = count_weight_decay(wc,wdp)
    cost_value = cost(xent,wd_classifier)


    #print (" \t compute derivative lost function ")

    dWC = weight_derivative(score, true_label, input, wc, wdp)
    dBC = bias_derivative(score, true_label,  bc)

    #print (" update weight and bias for softmax classifier ")
    new_WEC = update_weight(wc, dWC, lr )
    new_BEC = update_bias(bc, dBC, lr )

    #print ("change old matrix weight and vector bias to new value ... ...")

    return new_WEC, new_BEC, predict_class, xent
