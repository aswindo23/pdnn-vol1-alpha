import pandas as pd
import numpy as np
import math

def sigmoid_function(z):
    """
    fungsi sigmoid logistik 1 / 1 + exp(-x)
    :z merupakan kalkulasi network antara w,b dengan formulasi
    sigma W.x + b ; dimana x adalah nilai input dari input layer
    """
    return (1/(1+ math.exp(-z)))

def sigmoid_derivative(activation):
    """
    fungsi derivative dari fungsi logistik sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return activation * (1-activation)

def create_network( Weight, input, bias, n_hidden, n_input):
    """ Buat komputasi untuk menghitung jumlah network """

    network = np.zeros((n_hidden))
    sum = 0.0

    """
    print (("print n_input : %d ")%(n_input))
    print (("print n_hidden : %d ")%(n_hidden))
    print (("print n_bias : %d ")%(len(bias)))
    print ("-------------------------------------")
    """
    for i in range(0,n_hidden):

        for j in range(0,n_input + 1):
            if j==n_input :
                sum +=  bias[i]
            else:
                sum += Weight[i][j] * input[j]

        network[i] = sigmoid_function(sum)
        sum = 0.0
    return network


def create_network_output( Weight, input, bias, n_hidden, n_input):
    """ Buat komputasi untuk menghitung jumlah network """

    network = np.zeros((n_hidden))
    z = np.zeros((n_hidden))
    sum = 0.0

    """
    print (("print n_input : %d ")%(n_input))
    print (("print n_hidden : %d ")%(n_hidden))
    print (("print n_bias : %d ")%(len(bias)))
    print ("-------------------------------------")
    """
    for i in range(0,n_hidden):

        for j in range(0,n_input + 1):
            if j==n_input :
                sum +=  bias[i]
            else:
                sum += Weight[i][j] * input[j]

        network[i] = sigmoid_function(sum)
        z[i] = sum
        sum = 0.0
    return network, z


def hypotheses(z):
    """
    hypotheses merupakan fungsi untuk aktifasi pada layer output dimana
    hypotheses Hw,b^(x) = a dan a merupakan hasil kalkulasi dari perhitungan
    fungsi aktifasi sigmoid
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    """
    return sigmoid_function(z)

def weights_transpose(weight):

    weight_transpose = []
    for l in range(len(weight)-1,-1, -1):
        W = weight[l]
        weight_transpose.append( W.transpose((1,0)) )
    return weight_transpose

def cost_function(input_x,hypothesis_x):
    """
    cost function merupakan fungsi biaya yang mengkalkulasikan
    antara output target dengan output aktual dimana output actual adalah y
    dan output target dalah fungsi hypothesis
    :z merupakan kalkulasi dari jumlah network antara layer ke layer
    :y merupakan output aktual
    """
    return 0.5 * math.pow(abs(hypothesis_x - input_x ) , 2)

def count_weight_decay(weights, wdp):
    sum = 0
    for l in range(0,len(weights)):
        layer_weight = weights[l]
        for i in range(0,len(layer_weight[0])):
            for j in range(0,len(layer_weight)):
                sum += math.pow(layer_weight[j][i], 2)
    return (wdp*sum)/2

def rmse(mse):
    return math.sqrt(mse)

def overall_cost_function(m_cost_function, weight_decay, control_sparm, KL):
    sum = 0
    for i in range(0,len(m_cost_function)):
        sum += m_cost_function[i]

    mse = (sum/len(m_cost_function))
    sparsity_penalty = control_sparm * KL
    ocf = mse + weight_decay + sparsity_penalty

    return mse, sparsity_penalty, ocf


def sparsity_constrain( recontruction_x, hidden_decoder, hidden_encoder, layer_size):

    sparsity = np.zeros((layer_size,1))
    sum = 0

    for l in range(0,layer_size):
        if l == layer_size :
            for i in range(0,len(recontruction_x)):
                sum += recontruction_x[i]
            sparsity[l] = sum/len(recontruction_x)
            sum = 0
        elif l <= len(hidden_encoder) :
            for i in range(0,len(hidden_encoder)):
                hidden = hidden_encoder[i]
                for j in range(0,len(hidden_encoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
        else:
            for i in range(0,len(hidden_decoder)):
                hidden = hidden_decoder[i]
                for j in range(0, len(hidden_decoder[i])):
                    sum += hidden[j]
                sparsity[l] = sum/len(hidden)
                sum = 0
    return sparsity

def KullbackLeibler(sparsity, s_param):

    sum = 0
    for j in range(0,len(sparsity)):
        sum += (s_param * math.log(s_param/sparsity[j]) ) + ( (1-s_param)* math.log((1-s_param)/(1-sparsity[j])))
    return sum
